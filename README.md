# sequencer conda recipe

Home: https://github.com/epics-modules/sequencer

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS State Notation Language compiler and runtime sequencer
